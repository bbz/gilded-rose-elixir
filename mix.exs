defmodule GildedRose.Mixfile do
  use Mix.Project

  def project do
    # Gleam must go first
    [
      app: :gilded_rose,
      version: "0.0.1",
      elixir: "~> 1.0",
      erlc_paths: ["src", "gen"],
      compilers: [:gleam | Mix.compilers()],
      deps: [{:mix_gleam, "~> 0.1.0"}, {:gleam_decode, "~> 1.5"}]
    ]
  end
end
