defmodule GildedRoseTest do
  use ExUnit.Case

  @max_quality 50
  @min_quality 0
  @sulfuras "Sulfuras, Hand of Ragnaros"
  @aged_brie "Aged Brie"
  @backstage_pass "Backstage passes to a TAFKAL80ETC concert"
  @conjured_item "Conjured Mana Cake"

  test "Sulfuras never changes" do
    item = %Item{name: @sulfuras, sell_in: -999, quality: 999}

    {:ok, [gilded_item]} = :gilded_rose.update_quality([item])

    assert gilded_item == {@sulfuras, item.quality, item.sell_in}
  end

  test "Sell in always decreases (except for sulfuras)" do
    item = %Item{name: "", sell_in: 1, quality: 10}

    {:ok, [{_, _quality, sell_in}]} = :gilded_rose.update_quality([item])

    assert sell_in == 0
  end

  test "quality never drops below minimum (#{@min_quality})" do
    item = %Item{name: "", sell_in: 0, quality: @min_quality}

    {:ok, [{_kind, quality, _sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == @min_quality
  end

  test "quality never increases above maximum (#{@max_quality})" do
    item = %Item{name: @aged_brie, sell_in: -1, quality: @max_quality}

    {:ok, [{@aged_brie, quality, _sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == @max_quality

    item = %Item{name: @aged_brie, sell_in: 1, quality: @max_quality}

    {:ok, [{@aged_brie, quality, _sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == @max_quality

    item = %Item{
      name: @backstage_pass,
      sell_in: 1,
      quality: @max_quality
    }

    {:ok, [{@backstage_pass, quality, _sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == @max_quality
  end

  test "an unspecified item's quality decreases by one until sell in date is passed" do
    item = %Item{name: "", sell_in: 1, quality: @max_quality}

    {:ok, [{_kind, quality, sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == @max_quality - 1

    {:ok, [{_kind, quality, _sell_in}]} =
      :gilded_rose.update_quality([
        %Item{name: "", sell_in: sell_in, quality: quality}
      ])

    refute quality == @max_quality - 2
  end

  test "an unspecified item's quality decreases by two after sell in date" do
    item = %Item{name: "", sell_in: 0, quality: @max_quality}

    {:ok, [{_kind, quality, sell_in}]} =
      :gilded_rose.update_quality([
        item
      ])

    assert quality == @max_quality - 2

    {:ok, [{_kind, quality, _sell_in}]} =
      :gilded_rose.update_quality([
        %Item{name: "", sell_in: sell_in, quality: quality}
      ])

    assert quality == @max_quality - 4
  end

  test "an Aged Brie item's quality increases by one until the sell in date has passed" do
    item = %Item{name: @aged_brie, sell_in: 1, quality: 0}

    {:ok, [{@aged_brie, quality, sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == 1

    {:ok, [{@aged_brie, quality, _sell_in}]} =
      :gilded_rose.update_quality([
        %Item{name: @aged_brie, sell_in: sell_in, quality: quality}
      ])

    refute quality == 2
  end

  test "an Aged Brie item's quality increases by two after sell in date has passed" do
    item = %Item{name: @aged_brie, sell_in: 0, quality: 0}

    {:ok, [{@aged_brie, quality, sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == 2

    {:ok, [{@aged_brie, quality, _sell_in}]} =
      :gilded_rose.update_quality([
        %Item{name: @aged_brie, sell_in: sell_in, quality: quality}
      ])

    assert quality == 4
  end

  test "a Backstage item's quality never drops below minimum (#{@min_quality}) " do
    backstage = %Item{name: @backstage_pass, sell_in: -1, quality: 1}

    {:ok, [{@backstage_pass, quality, sell_in}]} = :gilded_rose.update_quality([backstage])

    {:ok, [{@backstage_pass, quality, _sell_in}]} =
      :gilded_rose.update_quality([
        %Item{
          name: @backstage_pass,
          sell_in: sell_in,
          quality: quality
        }
      ])

    assert quality == 0
  end

  test "a Backstage item's quality increases more, closer to the sell in date" do
    backstage = %Item{name: @backstage_pass, sell_in: 20, quality: 0}
    {:ok, [{@backstage_pass, quality, _sell_in}]} = :gilded_rose.update_quality([backstage])

    assert quality == 1

    backstage = %Item{name: @backstage_pass, sell_in: 11, quality: 0}
    {:ok, [{@backstage_pass, quality, _sell_in}]} = :gilded_rose.update_quality([backstage])

    assert quality == 2

    backstage = %Item{name: @backstage_pass, sell_in: 6, quality: 0}
    {:ok, [{@backstage_pass, quality, _sell_in}]} = :gilded_rose.update_quality([backstage])

    assert quality == 3
  end

  test "an conjured item's quality decreases by two until sell in date has passed" do
    item = %Item{name: @conjured_item, sell_in: 1, quality: @max_quality}

    {:ok, [{@conjured_item, quality, sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == @max_quality - 2

    {:ok, [{@conjured_item, quality, _sell_in}]} =
      :gilded_rose.update_quality([
        %Item{name: @conjured_item, sell_in: sell_in, quality: quality}
      ])

    refute quality == @max_quality - 4
  end

  test "an conjured item's quality decreases by four after the sell in date has passed" do
    item = %Item{name: @conjured_item, sell_in: 0, quality: @max_quality}

    {:ok, [{@conjured_item, quality, sell_in}]} = :gilded_rose.update_quality([item])

    assert quality == @max_quality - 4

    {:ok, [{@conjured_item, quality, _sell_in}]} =
      :gilded_rose.update_quality([
        %Item{name: @conjured_item, sell_in: sell_in, quality: quality}
      ])

    assert quality == @max_quality - 8
  end
end
