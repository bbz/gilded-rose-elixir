import decode.{
  Decoder, atom_field, decode_dynamic, int, list, map3, string, succeed, then,
}
import gleam/list

pub type ConstantItem {
  Sulfuras(name: String, quality: Int, sell_in: Int)
}

pub type DecreasingItem {
  ConjuredItem(name: String, quality: Int, sell_in: Int)
  RegularItem(name: String, quality: Int, sell_in: Int)
}

pub type IncreasingItem {
  AgedBrie(name: String, quality: Int, sell_in: Int)
  BackstagePass(name: String, quality: Int, sell_in: Int)
}

pub type GildedItem {
  Constant(ConstantItem)
  Decreasing(DecreasingItem)
  Increasing(IncreasingItem)
}

fn ex_item_decoder(name: String, quality: Int, sell_in: Int) -> GildedItem {
  case name {
    "Sulfuras, Hand of Ragnaros" -> Constant(Sulfuras(name, quality, sell_in))
    "Aged Brie" -> Increasing(AgedBrie(name, quality, sell_in))
    "Backstage passes to a TAFKAL80ETC concert" ->
      Increasing(BackstagePass(name, quality, sell_in))
    "Conjured Mana Cake" -> Decreasing(ConjuredItem(name, quality, sell_in))
    _ -> Decreasing(RegularItem(name, quality, sell_in))
  }
}

pub fn ex_item_list_decoder() -> Decoder(List(GildedItem)) {
  list(map3(
    ex_item_decoder,
    atom_field("name", string()),
    atom_field("quality", int()),
    atom_field("sell_in", int()),
  ))
}

pub fn decode_items(items) -> Result(List(GildedItem), String) {
  items
  |> decode_dynamic(ex_item_list_decoder())
}
