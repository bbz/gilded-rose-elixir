import item.{
  AgedBrie, BackstagePass, ConjuredItem, Constant, Decreasing, DecreasingItem, GildedItem,
  Increasing, IncreasingItem, RegularItem, Sulfuras, decode_items,
}
import gleam/result
import gleam/list
import gleam/int.{max, min}

pub const min_quality = 0

pub const max_quality = 50

type Unpacked =
  tuple(String, Int, Int)

pub fn update_quality(items) -> Result(List(Unpacked), String) {
  items
  |> decode_items()
  |> result.map(fn(items) {
    list.map(
      items,
      fn(item) {
        item
        |> age_item()
        |> update_item_quality()
        |> unpack_item()
      },
    )
  })
}

fn age_item(item: GildedItem) {
  case item {
    Constant(_) -> item
    Increasing(AgedBrie(name, quality, sell_in)) ->
      Increasing(AgedBrie(name, quality, sell_in - 1))
    Increasing(BackstagePass(name, quality, sell_in)) ->
      Increasing(BackstagePass(name, quality, sell_in - 1))
    Decreasing(RegularItem(name, quality, sell_in)) ->
      Decreasing(RegularItem(name, quality, sell_in - 1))
    Decreasing(ConjuredItem(name, quality, sell_in)) ->
      Decreasing(ConjuredItem(name, quality, sell_in - 1))
  }
}

fn update_item_quality(item: GildedItem) {
  case item {
    Constant(_) -> item
    Increasing(increasing_item) ->
      Increasing(increase_item_quality(increasing_item))
    Decreasing(regular_item) -> Decreasing(decrease_item_quality(regular_item))
  }
}

fn increase_item_quality(item: IncreasingItem) -> IncreasingItem {
  case item {
    AgedBrie(name, quality, sell_in) -> {
      let amount = case sell_in < 0 {
        True -> 2
        False -> 1
      }
      AgedBrie(name, sell_in, quality: increase_quality(quality, amount))
    }

    BackstagePass(name, quality, sell_in) ->
      case sell_in < 0 {
        True -> BackstagePass(name, sell_in, quality: min_quality)
        False -> {
          let amount = case sell_in < 6, sell_in < 11 {
            True, True -> 3
            False, True -> 2
            _, _ -> 1
          }
          BackstagePass(
            name,
            sell_in,
            quality: increase_quality(quality, amount),
          )
        }
      }
  }
}

fn decrease_item_quality(item: DecreasingItem) {
  case item {
    RegularItem(name, quality, sell_in) -> {
      let amount = case sell_in < 0 {
        True -> 2
        False -> 1
      }
      RegularItem(name, sell_in, quality: decrease_quality(quality, amount))
    }

    ConjuredItem(name, quality, sell_in) -> {
      let amount = case sell_in < 0 {
        True -> 4
        False -> 2
      }
      ConjuredItem(name, sell_in, quality: decrease_quality(quality, amount))
    }
  }
}

fn unpack_item(item: GildedItem) -> Unpacked {
  case item {
    Constant(Sulfuras(name, quality, sell_in)) -> tuple(name, quality, sell_in)
    Increasing(AgedBrie(name, quality, sell_in)) -> tuple(
      name,
      quality,
      sell_in,
    )
    Increasing(BackstagePass(name, quality, sell_in)) -> tuple(
      name,
      quality,
      sell_in,
    )
    Decreasing(ConjuredItem(name, quality, sell_in)) -> tuple(
      name,
      quality,
      sell_in,
    )
    Decreasing(RegularItem(name, quality, sell_in)) -> tuple(
      name,
      quality,
      sell_in,
    )
  }
}

fn increase_quality(quality: Int, amount: Int) {
  quality + amount
  |> min(max_quality)
}

fn decrease_quality(quality: Int, amount: Int) {
  quality - amount
  |> max(min_quality)
}
