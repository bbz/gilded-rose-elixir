defmodule Item do
  defstruct name: nil, sell_in: nil, quality: nil
end

defmodule GildedItem do
  @enforce_keys [:sell_in, :quality]

  defstruct kind: :regular, sell_in: nil, quality: nil
end
