# Gilded Rose Elixir
My personal solution to the [Gilded Rose Refactoring Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata#gilded-rose-refactoring-kata)  
The kata is based on the Elixir version and was initially implemented in [Elixir](https://gitlab.com/bbz/gilded-rose-elixir/-/commit/099756cf01182a050cbf6bd4ff9eb2a06f643edf).  
I've always been cursios about Gleam and I thought I'd take this opportunity to re-implement the solution in Gleam.

The original [repo](https://github.com/emilybache/GildedRose-Refactoring-Kata) can be found on Github, which also contains the [requirements](https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements.txt).
## Installation

Installation instructions for [Elixir](https://elixir-lang.org/install.html)  
Installation instructions for [Gleam](https://gleam.run/getting-started/#installing-gleam)  


## Install packages, Compile code/run tests
After installing Elixir and Gleam.  

**Install packages** 
```
mix deps.get
```
**Compile**
```bash
mix compile
```  
**Run tests** 
```bash
mix test
```

`



